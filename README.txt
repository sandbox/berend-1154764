This module will add a user to all roles in the shopping cart upon
completion of the checkout.

How to activate:
1. Add a role reference field to a product type.
2. Enable a specific role in a product.
3. In the manage display section for your content type you probably
   want to hide this role field in the output.
4. Create a rule that fires upon completion of the checkout. A default
   rules for this has been added. The event is "Complete the checkout
   process", the action is "Add user to all bought roles". Make sure
   it fires late (weight 4 or higher) as the user must have been
   created by this time.
