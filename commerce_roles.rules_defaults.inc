<?php

/**
 * @file
 * Default rule configurations for Roles.
 */

/**
 * Implements hook_default_rules_configuration().
 */
function commerce_roles_default_rules_configuration() {
  // Add a reaction rule to set roles upon checkout completion.
  $rule = rules_reaction_rule();

  $rule->label = t('Update user with roles bought');
  $rule->active = TRUE;

  $rule
    ->event('commerce_checkout_complete')
    ->action('commerce_roles_add_roles', array(
      'order:select' => 'order',
    ));

  // Adjust the weight so this rule executes after the order has been updated to
  // the proper user account.
  $rule->weight = 4;

  $rules['commerce_roles_set_roles'] = $rule;

  return $rules;
}
