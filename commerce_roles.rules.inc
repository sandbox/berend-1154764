<?php

/**
 * Rules integration for commerce roles.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_roles_rules_action_info() {
  $actions = array();

  $actions['commerce_roles_add_roles'] = array(
    'label' => t('Add user to all bought roles'),
    'parameter' => array(
      'order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce Cart'),
    'callbacks' => array(
      'execute' => 'commerce_roles_add_roles',
    ),
  );

  return $actions;
}

/**
 * Rules action: add all roles found in any of the bought products.
 */
function commerce_roles_add_roles($order) {
  $uid = $order->uid;
  $account = user_load ($uid);
  $roles = $account->roles;
  foreach ($order->commerce_line_items['und'] as $line_items) {
    $line_item_id = $order->commerce_line_items['und'][0]['line_item_id'];
    $line_item = commerce_line_item_load ($line_item_id);
    $product_id = $line_item->commerce_product['und'][0]['product_id'];
    if ($product_id) {
      $product = commerce_product_load ($product_id);
      if (isset ($product->field_role)) {
        $rid = $product->field_role['und'][0]['rid'];
        if ($rid)
          $roles[$rid] = $rid;
      }
    }
  }
  if (array_diff ($roles, $account->roles))
    user_save($account, array('roles' => $roles));
}
